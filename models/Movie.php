<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "movie".
 *
 * @property int $id
 * @property string $movieName
 * @property string $genre
 * @property int $minAge
 * @property int $movieRank
 */
class Movie extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'movie';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['minAge', 'movieRank'], 'integer'],
            [['movieName', 'genre'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'movieName' => 'Movie Name',
            'genre' => 'Genre',
            'minAge' => 'Min Age',
            'movieRank' => 'Movie Rank',
        ];
    }
}
