<?php

use yii\db\Migration;

/**
 * Handles the creation of table `movie`.
 */
class m170614_064201_create_movie_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('movie', [
            'id' => $this->primaryKey(),
            'movieName' => $this->string(),
            'genre' => $this->string(),
            'minAge' => $this->integer(),
            'movieRank' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('movie');
    }
}
