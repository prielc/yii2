<?php

use yii\db\Migration;

/**
 * Handles the creation of table `student`.
 */
class m170612_080710_create_student_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('student', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'identify' => $this->integer(),
            'age' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('student');
    }
}
